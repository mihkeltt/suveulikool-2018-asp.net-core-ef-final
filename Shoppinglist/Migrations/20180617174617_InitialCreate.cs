﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Shoppinglist.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Servings = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 150, nullable: false),
                    Brand = table.Column<string>(maxLength: 150, nullable: false),
                    PackageSize = table.Column<decimal>(nullable: true),
                    SalesUnit = table.Column<string>(maxLength: 5, nullable: false),
                    PriceEur = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Components",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 150, nullable: false),
                    Quantity = table.Column<decimal>(nullable: false),
                    OrderID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Components", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Components_Orders_OrderID",
                        column: x => x.OrderID,
                        principalTable: "Orders",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "ID", "Brand", "Name", "PackageSize", "PriceEur", "SalesUnit" },
                values: new object[,]
                {
                    { 1, "Chiquita", "Banaan", null, 0.95m, "kg" },
                    { 16, "Hispaania", "Apelsin Valencia", null, 0.89m, "kg" },
                    { 15, "Hispaania", "Kiivi korvis", 1m, 1.89m, "kg" },
                    { 14, "Premia", "Šokolaadi koorejäätis", 0.5m, 6.55m, "kg" },
                    { 13, "Jäts", "Vanilli koorejäätis", 0.5m, 6.23m, "kg" },
                    { 12, "Arbuusia", "Arbuus", null, 0.79m, "kg" },
                    { 11, "Peruu", "Mango", null, 2.19m, "kg" },
                    { 10, "Alma", "Maitsestamata jogurt", 0.38m, 2.97m, "kg" },
                    { 9, "YesRaspberry", "Vaarikad", 0.5m, 7.4m, "kg" },
                    { 8, "NoBananas", "Vaarikad", 0.5m, 6.30m, "kg" },
                    { 7, "Eesti", "Maasikad", 0.5m, 7.99m, "kg" },
                    { 6, "NoBananas", "Maasikad", 0.5m, 5.49m, "kg" },
                    { 5, "Alpro", "Mandlipiim", 1m, 2.29m, "kg" },
                    { 4, "Tere", "Piim", 1m, 0.59m, "kg" },
                    { 3, "Alma", "Piim", 1m, 0.79m, "kg" },
                    { 2, "Schmöko Toode", "Õun GreenPrince", null, 1.49m, "kg" },
                    { 17, "Cappy", "Õunamahl", 1m, 1.39m, "kg" },
                    { 18, "Aura", "Õunamahl", 1m, 0.99m, "kg" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Components_OrderID",
                table: "Components",
                column: "OrderID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Components");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Orders");
        }
    }
}
