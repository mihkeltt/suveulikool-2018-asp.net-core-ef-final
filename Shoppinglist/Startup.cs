﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NJsonSchema;
using NSwag.AspNetCore;
using Shoppinglist.Controllers;
using ShoppingList.Model;

namespace ShoppingList
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddCors();

            //var connection = @"Server=(localdb)\mssqllocaldb;Database=Shoppinglist;Trusted_Connection=True;ConnectRetryCount=0";
            var connection = @"Server=tcp:ngsuveylikool2018shoppinglistapi.database.windows.net,1433;Initial Catalog=ngsuveülikool2018shoppinglistapi;Persist Security Info=False;User ID=ngsuveylikool2018login;Password=XXX;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

            services.AddDbContext<ShoppingListContext>(options => options
                .UseSqlServer(connection,
                    sqlOptions => sqlOptions.MigrationsAssembly(typeof(ShoppingListContext).GetTypeInfo().Assembly.GetName().Name)));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseMvc();

            app.UseCors(builder => builder.AllowAnyOrigin());

            // Enable the Swagger UI middleware and the Swagger generator
            app.UseSwaggerUi(new List<Type>
            {
                typeof(OrderController),
                typeof(ShoppingListController),
                typeof(ProductController)
            }, settings =>
            {
                settings.GeneratorSettings.DefaultPropertyNameHandling =
                    PropertyNameHandling.CamelCase;
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
        }
    }
}
