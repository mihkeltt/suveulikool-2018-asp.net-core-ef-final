﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ShoppingList.Model;

namespace Shoppinglist.Controllers
{
    [Produces("application/json")]
    [Route("api/Product")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly ShoppingListContext _shoppingListContext;

        public ProductController(ShoppingListContext shoppingListContext)
        {
            _shoppingListContext = shoppingListContext;
        }

        [HttpGet("")]
        public ActionResult<List<Product>> GetAll()
        {
            return Enumerable.ToList<Product>(_shoppingListContext.Products);
        }
    }
}