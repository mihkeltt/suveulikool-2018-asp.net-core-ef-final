﻿using Microsoft.EntityFrameworkCore;

namespace ShoppingList.Model
{
	public class ShoppingListContext : DbContext
	{
		public ShoppingListContext(DbContextOptions<ShoppingListContext> options)
			: base(options)
		{ }

		public DbSet<Product> Products { get; set; }
		public DbSet<Order> Orders { get; set; }
		public DbSet<Component> Components { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.Entity<Product>()
				.HasData(
					new Product { ID = 1, Name = "Banaan", PackageSize = null, SalesUnit = "kg", PriceEur = 0.95m, Brand = "Chiquita" },
					new Product { ID = 2, Name = "Õun GreenPrince", PackageSize = null, SalesUnit = "kg", PriceEur = 1.49m, Brand = "Schmöko Toode" },
					new Product { ID = 3, Name = "Piim", PackageSize = 1m, SalesUnit = "kg", PriceEur = 0.79m, Brand = "Alma" },
					new Product { ID = 4, Name = "Piim", PackageSize = 1m, SalesUnit = "kg", PriceEur = 0.59m, Brand = "Tere" },
					new Product { ID = 5, Name = "Mandlipiim", PackageSize = 1m, SalesUnit = "kg", PriceEur = 2.29m, Brand = "Alpro" },
					new Product { ID = 6, Name = "Maasikad", PackageSize = 0.5m, SalesUnit = "kg", PriceEur = 5.49m, Brand = "NoBananas" },
					new Product { ID = 7, Name = "Maasikad", PackageSize = 0.5m, SalesUnit = "kg", PriceEur = 7.99m, Brand = "Eesti" },
					new Product { ID = 8, Name = "Vaarikad", PackageSize = 0.5m, SalesUnit = "kg", PriceEur = 6.30m, Brand = "NoBananas" },
					new Product { ID = 9, Name = "Vaarikad", PackageSize = 0.5m, SalesUnit = "kg", PriceEur = 7.4m, Brand = "YesRaspberry" },
					new Product { ID = 10, Name = "Maitsestamata jogurt", PackageSize = 0.38m, SalesUnit = "kg", PriceEur = 2.97m, Brand = "Alma" },
					new Product { ID = 11, Name = "Mango", PackageSize = null, SalesUnit = "kg", PriceEur = 2.19m, Brand = "Peruu" },
					new Product { ID = 12, Name = "Arbuus", PackageSize = null, SalesUnit = "kg", PriceEur = 0.79m, Brand = "Arbuusia" },
					new Product { ID = 13, Name = "Vanilli koorejäätis", PackageSize = 0.5m, SalesUnit = "kg", PriceEur = 6.23m, Brand = "Jäts" },
					new Product { ID = 14, Name = "Šokolaadi koorejäätis", PackageSize = 0.5m, SalesUnit = "kg", PriceEur = 6.55m, Brand = "Premia" },
					new Product { ID = 15, Name = "Kiivi korvis", PackageSize = 1m, SalesUnit = "kg", PriceEur = 1.89m, Brand = "Hispaania" },
					new Product { ID = 16, Name = "Apelsin Valencia", PackageSize = null, SalesUnit = "kg", PriceEur = 0.89m, Brand = "Hispaania" },
					new Product { ID = 17, Name = "Õunamahl", PackageSize = 1m, SalesUnit = "kg", PriceEur = 1.39m, Brand = "Cappy" },
					new Product { ID = 18, Name = "Õunamahl", PackageSize = 1m, SalesUnit = "kg", PriceEur = 0.99m, Brand = "Aura" }
				);
		}
	}
}