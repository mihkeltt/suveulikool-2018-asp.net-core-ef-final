﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace ShoppingList.Model
{
    public class Product
    {
        public int ID { get; set; }

        [Required]
        [MaxLength(150)]
        public string Name { get; set; }

        [Required]
        [MaxLength(150)]
        public string Brand { get; set; }

        public decimal? PackageSize { get; set; }

        [Required]
        [MaxLength(5)]
        public string SalesUnit { get; set; }

        public decimal PriceEur { get; set; }
    }

    public class Order
    {
        public int ID { get; set; }

        public int Servings { get; set; }

        public virtual List<Component> Components { get; set; }
    }

    public class Component
    {
        public int ID { get; set; }

        [Required]
        [MaxLength(150)]
        public string Name { get; set; }

        public decimal Quantity { get; set; }
    }

    [NotMapped]
    public class ShopList
    {
        public ShopList()
        {
            Items = new List<ShopListItem>();
        }

        public List<ShopListItem> Items { get; set; }

        public decimal TotalEur
        {
            get { return Items.Sum(i => i.TotalPriceEur); }
        }
    }

    [NotMapped]
    public class ShopListItem
    {
        public string Name { get; set; }

        public string Brand { get; set; }

        public decimal PackagePriceEur { get; set; }

        public decimal TotalPriceEur => PackagePriceEur * Packages;

        public int Packages { get; set; }

        public decimal PackageSize { get; set; }

        public string SalesUnit { get; set; }
    }
}